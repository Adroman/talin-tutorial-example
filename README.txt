Prerequisites:

Install Unity (latest version should work).
Create a new 2D project in Unity.
Download and import the 2D Game Kit package from Unity Asset store. This will take some time.
Import the Talin package from https://github.com/batu/Talin
	- downloading and opening TalinTutorials.unitypackage should be enough

Main tutorial:

The game is divided into 5 scenes/zones (besides Start, which is the main menu). You can locate them inside 2DGamekit/Scenes folder.
	- Zone1 is the starting area
	- Zone2 is the main area, where you need to collect 3 keys from 3 zones to enter Zone5
	- Zone3 is the area right to main area, it has breakable walls
	- Zone4 is more complex with multiple switches
	- Zone5 is the boss battle
Load the Zone1 scene and hit play.
Go to the semisolid platform where tutorial UI gets triggered.
	- it triggered instantly
Exit play mode.
Add KnowledgeBase prefab into scene.
Inside that object create an empty object with some creative skill name.
	- Add SkillAtom script to that object.
	- Give it some name and initial mastery (number closer to 1 means that the skill knowledge is more expected to be known).
Scene should have two InfoPost game objects for showing tutorial. Locate the one above semisolid platform (should be the second one).
	- Add a ProximityDetector prefab to it as a child object.
	- Set it's skill atom to the previously created game object with a SkillAtom sctipt.
	- Change it's On Reaction field to Decay.
	- Set it's target to Ellen game object (that's the player).
	- Add ActivateGameObjectHint script to it's detector, set its Hint Activation Mastery Threshold to some low number and drag BG game object (DialogueCanvas' child object) as an object to be activated
	- Disable the DialogueCanvas, UIBlur and BG game objects.
Locate the Ellen game object and add MultipleKeyInputDetector prefab to it.
	- Set it's Skill atom to same object as used for proximity detector.
	- Leave On Reaction field to Excercise.
	- Set its Weight to 1
	- Set its Keys to [Space, S] array (e.g. set it's Size to 2, Element 0 to Space and Element 1 to S)
Try to run the game now, go to semisolid platform and wait.
	- The skill will decay and when it hits its threshold, the UI hint will popup.
	- If you press Space+S on that semisolid platform, skill will go up to 1.

Homework:

Create a hint for basic controls
	- moving the first InfoPost is recommended

Try to create more detectors / hints on other scenes (unfortunately not everything is preserved)


